/*Load the expressjs module into our application and saved it in a variable called express */

//  C - Create			POST
//  R- Read				GET
//  U - Update			PUT / PATCH
//  D - Delete			DELETE


const express =  require("express");
/* 
    This created an application that uses express and store it as app
    app is our server
    app - containes express module
*/
const app = express();

const port = 4000;

// MIDDLEWARE if the stream of data was entered it will automatically convert the express.json will handle it. It will automatically parse the data stream
// automatic parsing
// Buffer data sample 35c 25a 45d
app.use(express.json())

// MOCK DATA
let users = [
    {
        username: "BMadrigal",
        email: "fateReader@gmail.com",
        password: "dontTalkAboutMe"
    },
    {
        username: "Luisa",
        email: "stronggirl@gmail.com",
        password: "pressure"
    }
];

let items = [
    {
        name: "roses",
        price: 170,
        isActive: true
    },
    {
        name: "tulips",
        price: 250,
        isActive: true
    },
];

// GET METHOD / READ
// app.get(<endpoint>, <function for req and res>)
app.get('/', (req, res) => {

    res.send('Hello from my first expressJS API')
    // res.jsoln(users);
    // res.status(200).send(''Hello from my first expressJS API')
});

// MINI ACTIVITY

/*Mini Activity: 5 mins

    >> Create a get route in Expressjs which will be able to send a message in the client:

        >> endpoint: /greeting
        >> message: 'Hello from Batch182-surname'

    >> Test in postman
    >> Send your response in hangouts*/


    // SOLUTION
    app.get('/greeting', (req, res) => {

        res.send('Hello from Batch182-Ordonio')
    });

// retrieval of the mock database
app.get('/users', (req,res) => {

    res.send(users); //stringified it already for you
    // res.json(users); (other way it does simplying method under response/ alternative)
});


// POST METHOD // CREATE
// added new user
app.post('/users', (req, res) => {

    console.log(req.body); // result: {}

    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }

    // push newUser into users array
    users.push(newUser);
    console.log(users);

    // send the updated users array in the client
    res.send(users);
});

// DELETE METHOD 
app.delete('/users', (req, res) => {

    users.pop();
    console.log(users);

    // send the updated users array
    res.send(users);
});



// PUT METHOD //UPDATE
// Update Password
// update user's method
// :index - this is called a wildcard. It can specify the users
// in postman: url: localhost:4000/users/0
app.put('/users/:index', (req, res) => { 

    console.log(req.body)


    //0
    // an object that contains the value of URL params
    // whatever the users after the /users/:index is gonna get abc
    console.log(req.params) 

    // from String //{ index: '0' }
    // ['0'] turns into [0]
    // parseInt the value of the number coming from req.params
    let index = parseInt(req.params.index);

    // users[0].password //reassigning into the new value
    users[index].password = req.body.password;

    res.send(users[index]);
});

// Mini Activity
    /* 
    
Mini-Activity: 5 mins

    >> endpoint: update/:index
    >> method: PUT

        >> Update a user's username
        >> Specify the user using the index in the params
        >> Put the updated username in request body
        >> Send the updated user as a response
        >> Test in Postman
        >> Send your screenshots in Hangouts


*/

// SOLUTION
// PUT METHOD //UPDATE
// Update the Username
app.put('/users/update/:index', (req, res) => {
    let index = parseInt(req.params.index);

    // users[index].username = req.body.username;

    users[index] = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    }


//     let updateUsers = {
//         username: req.body.username,
//         email: req.body.email,
//         password: req.body.password
//         }
//         users[index] = updateUsers

//     res.send(users[index]);
});


// Retrieval of single user
app.get('/users/getSingleUser/:index', (req, res) => {

    //0
    // an object that contains the value of URL params
    console.log(req.params) //result {index: '1'}
    // let req.params = {index: '1'}

    let index = parseInt(req.params.index)
    // parseInt('1')

    console.log(index); //result: 1 //converted into integer

    res.send(users[index]);
    console.log(users[index]);
});
// ACTIVITY # 1 INSTRUCTIONS
/* Endpoint: /items

>> Create a new route to get and send items array in the client (GET ALL ITEMS)

>> Create a new route to create and add a new item object in the items array (CREATE ITEM)
    >> send the updated items array in the client
    >> check the post method route for our users for reference

>> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
    >> pass the index number of the item that you want to update in the request params
    >> add the price update in the request body
    >> reassign the new price from our request body
    >> send the updated item in the client

>> Create a new collection in Postman called s33-activity
>> Save the Postman collection in your s33-s34 folder

*/


// SOLUTION ACTIVITY #1
// Retrieval of all Items
// GET METHOD
app.get('/items', (req, res) => {

    // stringified the iems
    res.send(items);
});

// Add Item new item/ Update
// POST METHOD
app.post('/items', (req, res) => {

    console.log(req.body); // result: {}

    // let reqBody = req.body (then change the req.body into reqBody inder the new Items)

    let newItems = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    }

    // push newItems into users array
    items.push(newItems);
    console.log(items);

    // send the updated items array in the client
    res.send(items);
});

// update item (Price)
app.put('/items/:index', (req, res) => {
    let index = parseInt(req.params.index);

    items[index].price = req.body.price;

    res.send(items[index]);
});


// SOLUTION ACTIVITY #2
/*
Activity #2
>> Create a new route with '/items/getSingleItem/:index' endpoint.
    >> This route should allow us to GET the details of a single item.
        -Pass the index number of the item you want to get via the url as url params in your postman.
        -http://localhost:4000/items/getSingleItem/<indexNumberOfItem>
   
    >> Send the request and back to your api
        -get the data from the url params.
        -send the particular item from our array using its index to the client. // done

>> Create a new route to update an item with '/items/archive/:index' endpoint.    (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to de-activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to false.
        -send the updated item in the client

>> Create a new route to update an item with '/items/activate/:index' endpoint. (WITHOUT USING REQUEST BODY)
    >> This route should allow us to UPDATE the isActive property of our product.
        -Pass the index number of the item you want to activate via the url as url params.
        -Access the particular item with its index. Access the isActive property of the item and re-assign it to true.
        -send the updated item in the client

>> Create a new collection in Postman called s34-activity
>> Save the Postman collection in your s33-s34 folder

*/
//CREATE 
app.get('/items/getSingleItem/:index', (req, res) => {

    let index = parseInt(req.params.index)
    res.send(items[index]);
});


// UPDATE with /items/archive/:index'
app.put('/items/archive/:index', (req, res) => {
    // converting into number
    let itemIndex = parseInt(req.params.index);
    items[itemIndex].isActive = false;
    res.send(items[itemIndex]);
});

// UPDATE with items/activate/:index'
app.put('/items/activate/:index', (req, res) => {
    items[+req.params.index].isActive = true;
    res.send(items[+req.params.index])
});

// users[index] = {
//     username: req.body.username,
//     email: req.body.email,
//     password: req.body.password
// }

// res.send(items[+req.params.index]);



// PORT
app.listen(port, () => console.log(`Server is running at port ${port}`))